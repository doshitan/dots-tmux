# tmux configuration
#
# Various bits pulled from all over:
#   * https://github.com/tangledhelix/dotfiles/blob/master/tmux.conf
#
# TODO
#   * Enable/style activity alerts

setw -g mode-keys vi
bind-key -T copy-mode-vi v send -X begin-selection
bind-key -T copy-mode-vi V send -X rectangle-toggle
bind-key -T copy-mode-vi y send -X copy-selection-and-cancel

set -g default-terminal "screen-256color"

set -g history-limit 10000

# easily toggle synchronization (mnemonic: e is for echo)
# sends input to all panes in a given window.
bind e setw synchronize-panes on
bind E setw synchronize-panes off

# Clear screen
bind C send-keys -R

# https://bitbucket.org/lyro/evil/issue/69/delay-between-esc-or-c-and-modeswitch
set -s escape-time 0

# Mouse #####################################################################
# http://tangledhelix.com/blog/2012/07/16/tmux-and-mouse-mode/

set -g mouse on

# Toggle mouse on
bind m \
    set -g mouse on \;\
    display 'Mouse: ON'

# Toggle mouse off
bind M \
    set -g mouse off \;\
    display 'Mouse: OFF'


# Theme #####################################################################

# Center the window list
set -g status-justify centre

set -g status-style bg=default

setw -g window-status-format "#[bg=white, fg=colour8] #I #[bg=colour8, fg=white] #W "
setw -g window-status-style dim

setw -g window-status-current-format "#[bg=colour7, fg=black] #I #[bg=colour15, fg=black] #W "
setw -g window-status-current-style bright

# remove administrative debris (hostname, time) in status bar
set -g status-left "#[bg=white, fg=colour8, bold] ❐ #S "
set -g status-left-length 20
set -g status-right ''


# Bindings ##################################################################

unbind r
bind r source-file ~/.tmux.conf \; display-message "Configuration reloaded"

# Smart pane switching with awareness of vim splits
# https://github.com/christoomey/vim-tmux-navigator
is_vim='echo "#{pane_current_command}" | grep -iqE "(^|\/)g?(view|n?vim?)(diff)?$"'
bind -n C-h if-shell "$is_vim" "send-keys C-h" "select-pane -L"
bind -n C-j if-shell "$is_vim" "send-keys C-j" "select-pane -D"
bind -n C-k if-shell "$is_vim" "send-keys C-k" "select-pane -U"
bind -n C-l if-shell "$is_vim" "send-keys C-l" "select-pane -R"
bind -n C-\\ if-shell "$is_vim" "send-keys C-\\" "select-pane -l"

# Restore clear screen behavior to <prefix> C-l
bind C-l send-keys 'C-l'

# Session management
bind-key S new-session
# Copied from http://unix.stackexchange.com/a/58616
bind-key X confirm-before -p "Kill #S (y/n)?" "run-shell 'tmux switch-client -n \\\; kill-session -t \"\$(tmux display-message -p \"#S\")\"'"


# Local config ##############################################################

if-shell "[ -f ~/.tmux.conf.local ]" 'source ~/.tmux.conf.local'
